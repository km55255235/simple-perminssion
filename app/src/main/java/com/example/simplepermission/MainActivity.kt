package com.example.simplepermission

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.simplepermission.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Glide.with(this)
            .load("https://images.deliveryhero.io/image/fd-kh/Products/1944603.jpg?width=%s")
            .fitCenter()
            .into(binding.imageView)

        binding.btnAction.setOnClickListener {
            val permissionLocationCheck = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            val permissionContact = checkSelfPermission(Manifest.permission.READ_CONTACTS)

            if (permissionLocationCheck == PackageManager.PERMISSION_GRANTED && permissionContact == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Location DIIZINKAN", Toast.LENGTH_LONG).show()
                //Tambahkan logic apa yang ingin kalian lakukan bila izinnya di ACC User
            } else {
                Toast.makeText(this, "Permission Location DITOLAK", Toast.LENGTH_LONG).show()
                requestLocationPermission()
            }
        }
    }

    fun readAllContacts(){
        val phones = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            null
        )
        val arrayList = arrayListOf<String>()
        while (phones!!.moveToNext()) {
            val name =
                phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            val phoneNumber =
                phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
            arrayList.add(phoneNumber)
        }
        phones!!.close()
    }
    fun requestLocationPermission() {
        requestPermissions(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_CONTACTS
            ), 101
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            101 -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION
                ) {
                    Toast.makeText(this, "Permission Location DIIZINKAN", Toast.LENGTH_LONG).show()
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION
                ) {
                    Toast.makeText(this, "Permission Location Ditolak", Toast.LENGTH_LONG).show()
                } else if (grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    permissions[1] == Manifest.permission.READ_CONTACTS
                ) {
                    Toast.makeText(this, "Permission Contacts DIIZINKAN", Toast.LENGTH_LONG).show()
                } else if (grantResults[1] == PackageManager.PERMISSION_DENIED &&
                    permissions[1] == Manifest.permission.READ_CONTACTS
                ) {
                    Toast.makeText(this, "Permission Contacts DIIZINKAN", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, "Permission DITOLAK", Toast.LENGTH_LONG).show()
                }
            }

            else -> {
                Toast.makeText(this, "Permission Ditolak", Toast.LENGTH_LONG).show()
            }
        }
    }
}